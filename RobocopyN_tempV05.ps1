﻿# Function Declarations

function Transfer ([string]$IsReplacement){
    if ($IsReplacement -eq "Y"){

        $Com1 = Read-Host "Please enter the computer name or IP address of the computer you want to pull data from"
        $User = Read-Host "Please enter the client's username"

        robocopy "\\$Com1\c$\Users\$User.D_ABSA\Documents\" "C:\Users\Public\Documents\Documents" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\Desktop" "C:\Users\Public\Documents\Desktop" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\Downloads" "C:\Users\Public\Documents\Downloads" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\Favorites" "C:\Users\Public\Documents\Favorites" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\Pictures" "C:\Users\Public\Documents\Pictures" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\Videos" "C:\Users\Public\Documents\Videos" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\Music" "C:\Users\Public\Documents\Music" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\AppData\Local\Google" "C:\Users\Public\Documents\AppData\Local\Google" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\AppData\LocalLow" "C:\Users\Public\Documents\AppData\LocalLow" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\AppData\Roaming" "C:\Users\Public\Documents\AppData\Roaming" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User.D_ABSA\" "C:\Users\Public\Documents" /IF *.* /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
    }elseif ($IsReplacement -eq "N"){

        $Com1 = Read-Host "Please enter the computer name or IP address of the computer you want to push data to"
        $User = Read-Host "Please enter the client's username"

        robocopy "C:\Users\$User.D_ABSA\Documents\Documents" "\\$Com1\c$\Users\Public\Documents\Documents" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\Desktop" "\\$Com1\c$\Users\Public\Documents\Desktop" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\Downloads" "\\$Com1\c$\Users\Public\Documents\Downloads" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\Favorites" "\\$Com1\c$\Users\Public\Documents\Favorites" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\Pictures" "\\$Com1\c$\Users\Public\Documents\Pictures" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\Videos" "\\$Com1\c$\Users\Public\Documents\Videos" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\Music" "\\$Com1\c$\Users\Public\Documents\Music" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\AppData\Local\Google" "\\$Com1\c$\Users\Public\Documents\AppData\Local\Google" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\AppData\LocalLow" "\\$Com1\c$\Users\Public\Documents\AppData\LocalLow" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User.D_ABSA\Documents\AppData\Roaming" "\\$Com1\c$\Users\Public\Documents\AppData\Roaming" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "c:\Users\$User.D_ABSA\Documents\" "\\$Com1\c$\Users\Public\Documents\" /IF *.* /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
    }   
}

# BODY OF THE SCRIPT

$Admin = Read-Host "Please enter your AB number"
$CallRef = Read-Host "Please enter the call ref of this incident/request"
$Desktop = Read-Host "Please enter the machine name or IP of the desktop you use on a daily basis(To get a notification when complete in Documents folder -> Robot Copy)"
$Retry = 0

while ($Retry -le 3) {
    $Answer = Read-Host "Are you using the replacement laptop to transfer? (please answer Y Or N)"

    if ($Answer -eq "Y" -or $Answer -eq "N") { Break }

    $Retry += 1
    
    if ($Retry -eq 3){
        write-host "You're wasting company time."
        Exit
    }elseif ($Retry -gt 0) { Write-Host "Incorrect Entry." }   
}

Transfer $Answer

md -path "\\$Desktop\c$\users\$Admin\documents\Robot Transfer"
New-Item -ItemType file -Path "\\$Desktop\C$\users\$Admin\Documents\Robot Transfer\$CallRef Report.txt"

Write-Host "`n`nTransfer is now complete!"