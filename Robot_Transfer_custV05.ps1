﻿Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force 

# Function Declarations

function Transfer ([string]$IsReplacement){
    if ($IsReplacement -eq "Y"){

        $Com1 = Read-Host "Please enter the computer name or IP address of the computer you want to pull data from"
        $User = Read-Host "Please enter the client's username"

        robocopy "\\$Com1\c$\Users\$User\Documents" "C:\Users\$User\Documents" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Desktop" "C:\Users\$User\Desktop" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Downloads" "C:\Users\$User\Downloads" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Favorites" "C:\Users\$User\Favorites" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Pictures" "C:\Users\$User\Pictures" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Videos" "C:\Users\$User\Videos" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Music" "C:\Users\$User\Music" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\AppData\Local\Google" "C:\Users\$User\AppData\Local\Google" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\AppData\LocalLow" "C:\Users\$User\AppData\LocalLow" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\AppData\Roaming" "C:\Users\$User\AppData\Roaming" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\" "C:\Users\$User\" /IF *.* /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
    }elseif ($IsReplacement -eq "N"){

        $Com1 = Read-Host "Please enter the computer name or IP address of the computer you want to push data to"
        $User = Read-Host "Please enter the client's username"

        robocopy "C:\Users\$User\Documents" "\\$Com1\c$\Users\$User\Documents" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Desktop" "\\$Com1\c$\Users\$User\Desktop" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Downloads" "\\$Com1\c$\Users\$User\Downloads" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Favorites" "\\$Com1\c$\Users\$User\Favorites" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Pictures" "\\$Com1\c$\Users\$User\Pictures" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Videos" "\\$Com1\c$\Users\$User\Videos" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Music" "\\$Com1\c$\Users\$User\Music" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\AppData\Local\Google" "\\$Com1\c$\Users\$User\AppData\Local\Google" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\AppData\LocalLow" "\\$Com1\c$\Users\$User\AppData\LocalLow" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\AppData\Roaming" "\\$Com1\c$\Users\$User\AppData\Roaming" /E /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "c:\Users\$User\" "\\$Com1\c$\Users\$User\" /IF *.* /COPYALL /ZB /XO /MT:128 /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
    }   
}

# BODY OF THE SCRIPT

$Admin = Read-Host "Please enter your AB number"
$CallRef = Read-Host "Please enter the call ref of this incident/request"
$Desktop = Read-Host "Please enter the machine name or IP of the desktop you use on a daily basis(To get a notification when complete in Documents folder -> Robot Copy)"
$Retry = 0

while ($Retry -le 3) {
    $Answer = Read-Host "Are you using the replacement laptop to transfer? (please answer Y Or N)"

    if ($Answer -eq "Y" -or $Answer -eq "N") { Break }

    $Retry += 1
    
    if ($Retry -eq 3){
        write-host "You're wasting company time."
        Exit
    }elseif ($Retry -gt 0) { Write-Host "Incorrect Entry." }   
}

Transfer $Answer

md -path "\\$Desktop\c$\users\$Admin\documents\Robot Transfer"
New-Item -ItemType file -Path "\\$Desktop\C$\users\$Admin\Documents\Robot Transfer\$CallRef Report.txt"

Write-Host "`n`nTransfer is now complete!"