﻿Set-ExecutionPolicy Bypass -Scope CurrentUser -Force 

# Function Declarations

function Recover ([string]$IsReplacement){
    if ($IsReplacement -eq "Y"){

        $Com1 = Read-Host "Please enter the computer name or IP address of the Backup server"
        $User = Read-Host "Please enter the client's username"

        robocopy "\\$Com1\C$\Users\-admin-abbma91\Documents\Backups\$User\Documents\" "c:\Users\Public\Documents\Documents" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Desktop\" "C:\Users\Public\Documents\Desktop\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Downloads" "C:\Users\Public\Documents\Downloads\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Favorites" "C:\Users\Public\Documents\Favorites\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Pictures" "C:\Users\Public\Documents\Pictures\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Videos" "C:\Users\Public\Documents\Videos\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Music" "C:\Users\Public\Documents\Music\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\AppData\Local\Google" "C:\Users\Public\Documents\AppData\Local\Google\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\AppData\LocalLow" "C:\Users\Public\Documents\AppData\LocalLow\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\AppData\Roaming" "C:\Users\Public\Documents\AppData\Roaming\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
    }elseif ($IsReplacement -eq "N"){

        $Com1 = Read-Host "Please enter the computer name or IP address of the client's workstation."
        $User = Read-Host "Please enter the client's username"

        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\Documents" "\\$Com1\c$\Users\Public\Documents\Documents\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\Desktop" "\\$Com1\c$\Users\Public\Documents\Desktop\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\Downloads" "\\$Com1\c$\Users\Public\Documents\Downloads" /E /COPYALL /CREATE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\Favorites" "\\$Com1\c$\Users\Public\Documents\Favorites\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\Pictures" "\\$Com1\c$\Users\Public\Documents\Pictures\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\Videos" "\\$Com1\c$\Users\Public\Documents\Videos\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\Music" "\\$Com1\c$\Users\Public\Documents\Music\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\AppData\Local\Google" "\\$Com1\c$\Users\Public\Documents\AppData\Local\Google\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\AppData\LocalLow" "\\$Com1\c$\Users\Public\Documents\AppData\LocalLow\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\-admin-abbma91\Documents\Backups\$User\AppData\Roaming" "\\$Com1\c$\Users\Public\Documents\AppData\Roaming\" /E /COPYALL /PURGE /ZB /XO /EFSRAW /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
    }
}

# Body of the script

$Admin = Read-Host "Please enter your AB number"
$CallRef = Read-Host "Please enter the call ref of this incident/request"
$Desktop = Read-Host "Please enter the machine name or IP of the desktop you use on a daily basis(To get a notification when complete in Documents folder -> Robot Copy)"
$Retry = 0

while ($Retry -le 3) {
    $Answer = Read-Host "Are you using the client's workstation to do a data recovery? (please answer Y Or N)" 

    if ($Answer -eq "Y" -or $Answer -eq "N") { Break }

    $Retry += 1
    
    if ($Retry -eq 3){
        write-host "You're wasting company time."
        Exit
    }elseif ($Retry -gt 0) { Write-Host "Incorrect Entry." }
}

Recover $Answer

md -path "\\$Desktop\c$\users\$Admin\documents\Robot Transfer"
New-Item -ItemType file -Path "\\$Desktop\C$\users\$Admin\Documents\Robot Transfer\$CallRef Report.txt"

Write-Host "`n`nBackup is now complete!"