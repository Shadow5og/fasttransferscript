﻿Set-ExecutionPolicy Bypass -Scope CurrentUser -Force 

# Function Declarations

function Backup ([string]$IsReplacement){
    if ($IsReplacement -eq "N"){

        $Com1 = Read-Host "Please enter the computer name or IP address of the client's computer"
        $User = Read-Host "Please enter the client's username"

        robocopy "\\$Com1\c$\Users\$User\Documents" "C:\Users\-admin-abbma91\Documents\Backups\$User\Documents\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Desktop" "C:\Users\-admin-abbma91\Documents\Backups\$User\Desktop\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Downloads" "C:\Users\-admin-abbma91\Documents\Backups\$User\Downloads\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Favorites" "C:\Users\-admin-abbma91\Documents\Backups\$User\Favorites\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Pictures" "C:\Users\-admin-abbma91\Documents\Backups\$User\Pictures\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Videos" "C:\Users\-admin-abbma91\Documents\Backups\$User\Videos\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\Music" "C:\Users\-admin-abbma91\Documents\Backups\$User\Music\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\AppData\Local\Google" "C:\Users\-admin-abbma91\Documents\Backups\$User\AppData\Local\Google\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\AppData\LocalLow" "C:\Users\-admin-abbma91\Documents\Backups\$User\AppData\LocalLow\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "\\$Com1\c$\Users\$User\AppData\Roaming" "C:\Users\-admin-abbma91\Documents\Backups\$User\AppData\Roaming\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
    }elseif ($IsReplacement -eq "Y"){

        $Com1 = Read-Host "Please enter the computer name or IP address of the computer you want to upload data to"
        $User = Read-Host "Please enter the client's username"

        robocopy "C:\Users\$User\Documents" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Documents\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Desktop" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Desktop\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Downloads" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Downloads" /E /COPYALL /CREATE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Favorites" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Favorites\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Pictures" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Pictures\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Videos" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Videos\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\Music" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\Music\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\AppData\Local\Google" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\AppData\Local\Google\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\AppData\LocalLow" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\AppData\LocalLow\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
        robocopy "C:\Users\$User\AppData\Roaming" "\\$Com1\c$\Users\-admin-abbma91\Documents\Backups\$User\AppData\Roaming\" /E /COPYALL /PURGE /ZB /XO /MT:128 /MIR /R:2 /W:2 /NDL /ETA /LOG+:"\\$Desktop\c$\users\$Admin\documents\Robot Transfer\Logs\$CallRef Transfered $User Data.txt"
    }
}

# Body of the script

$Admin = Read-Host "Please enter your AB number"
$CallRef = Read-Host "Please enter the call ref of this incident/request"
$Desktop = Read-Host "Please enter the machine name or IP of the desktop you use on a daily basis(To get a notification when complete in Documents folder -> Robot Copy)"
$Retry = 0

while ($Retry -le 3) {
    $Answer = Read-Host "Are you using the client's laptop to do a data backup? (please answer Y Or N)" 

    if ($Answer -eq "Y" -or $Answer -eq "N") { Break }

    $Retry += 1
    
    if ($Retry -eq 3){
        write-host "You're wasting company time."
        Exit
    }elseif ($Retry -gt 0) { Write-Host "Incorrect Entry." }
}

Backup $Answer

if ( -not ("\\$Desktop\c$\users\$Admin\documents\Robot Transfer")) {
    md -path "\\$Desktop\c$\users\$Admin\documents\Robot Transfer"
}

New-Item -ItemType file -Path "\\$Desktop\C$\users\$Admin\Documents\Robot Transfer\$CallRef Report.txt"

Write-Host "`n`nBackup is now complete!"